#!/usr/bin/env sh

set -v
set -e

CONTAINER_NAME="$1"
CONTAINER_TEST_IMAGE="$2"

docker rm -f ${CONTAINER_NAME} 2>&1 > /dev/null || true

docker run --name ${CONTAINER_NAME} -d ${CONTAINER_TEST_IMAGE}

# tests
docker exec -t ${CONTAINER_NAME} ash -c 'id bender | grep "uid=2001(bender) gid=2001(bender) groups=2001(bender),2001(bender)"'
docker exec -t ${CONTAINER_NAME} ash -c 'apk add --update curl'
docker exec -t ${CONTAINER_NAME} ash -c 'curl -s http://localhost | grep "We&rsquo;ll be back soon!"'

# clean up
docker rm -f ${CONTAINER_NAME}
