#! /bin/sh

set -e

if [ $# -eq 0 ]; then
  nginx -g "daemon off;"
else
  env $@
fi
