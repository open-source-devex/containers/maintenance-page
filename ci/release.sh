#!/usr/bin/env sh

set -e

if [ -f ".env" ]; then
  source .env
fi

set -v

if [ -z "${CONTAINER_TEST_IMAGE}" ]; then
  CONTAINER_TEST_IMAGE=test-image
fi

if [ -z "${CONTAINER_RELEASE_IMAGE}" ]; then
  CONTAINER_RELEASE_IMAGE=release-image
fi

docker pull ${CONTAINER_TEST_IMAGE}
docker tag ${CONTAINER_TEST_IMAGE} ${CONTAINER_RELEASE_IMAGE}
docker push ${CONTAINER_RELEASE_IMAGE}
